from django.db import models

# Create your models here.

from django.db import models

class Empleado(models.Model):
    id = models.AutoField(primary_key=True)
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    tipo_identificacion = models.CharField(max_length=5)
    identificacion = models.CharField(max_length=20, unique=True)
    fecha_ingreso = models.DateField()
    salario_mensual = models.DecimalField(max_digits=10, decimal_places=2)
    cargo = models.CharField(max_length=100)
    departamento = models.CharField(max_length=100)

class Telefono(models.Model):
    id = models.AutoField(primary_key=True)
    tipo = models.CharField(max_length=4)
    numero = models.CharField(max_length=20)
    indicativo = models.CharField(max_length=10, blank=True, null=True)
    empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE)

class Email(models.Model):
    id = models.AutoField(primary_key=True)
    email = models.EmailField(unique=True)
    empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE)
