from django.urls import path, re_path, include
#from rest_framework.documentation import include_docs_urls
from rest_framework import routers
from empleados import views
#from .views import login, register, profile, mail, menu, lista_empleados, lista_telefonos,lista_emails, crear_empleados, crear_telefonos, crear_emails, eliminar_empleados, eliminar_telefonos, eliminar_emails

router = routers.DefaultRouter()
router.register(r'empleados',  views.CrudEmpleados, 'empleados')
router.register(r'telefonos',  views.CrudTelefonos, 'telefonos')
router.register(r'emails',  views.CrudEmails, 'emails')

urlpatterns = [

    path("", include(router.urls)),
    #path('docs/', include_docs_urls(title="Empleados API"))

    # path('', menu),
    # path('empleados/', lista_empleados),
    # path('telefonos/', lista_telefonos),
    # path('emails/', lista_emails),
    # path('nuevo_empleado/', crear_empleados, name='crear_empleados'),
    # path('eliminar_empleado/<int:id_empleado>/', eliminar_empleados, name='eliminar_empleados'),
    # path('nuevo_telefono/', crear_telefonos, name='crear_telefonos'),
    # path('eliminar_telefono/<int:id_telefono>/', eliminar_telefonos, name='eliminar_telefonos'),
    # path('nuevo_email/', crear_emails, name='crear_emails'),
    # path('eliminar_email/<int:id_email>/', eliminar_emails, name='eliminar_emails'),
    # re_path('login',login),
    # re_path('register',register),
    # re_path('profile',profile),

]