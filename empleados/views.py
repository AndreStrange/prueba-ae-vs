from django.shortcuts import render, redirect
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.conf import settings
from django.contrib import messages
from .models import Empleado, Telefono, Email
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import UserSerializer
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework import status
from django.shortcuts import get_object_or_404
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from rest_framework import viewsets

from .serializers import EmpleadoSerializer, TelefonoSerializer, EmailSerializer

from .models import Empleado, Telefono, Email

class CrudEmpleados(viewsets.ModelViewSet):
    serializer_class = EmpleadoSerializer
    queryset = Empleado.objects.all()

class CrudTelefonos(viewsets.ModelViewSet):
    serializer_class = TelefonoSerializer
    queryset = Telefono.objects.all()

class CrudEmails(viewsets.ModelViewSet):
    serializer_class = EmailSerializer
    queryset = Email.objects.all()




@api_view(['POST'])
def login(request):

    user = get_object_or_404(User, username=request.data['username'])
    if not user.check_password(request.data['password']):
        return Response({"error": "Invalid password"}, status=status.HTTP_400_BAD_REQUEST)

    token, created = Token.objects.get_or_create(user=user)
    serializer = UserSerializer(instance=user)
    
    return Response({"token": token.key, "user": serializer.data}, status=status.HTTP_200_OK)

@api_view(['POST'])
def register(request):
    
    serializer = UserSerializer(data=request.data)

    if serializer.is_valid():
        serializer.save()

        user = User.objects.get(username=serializer.data['username'])
        user.set_password(serializer.data['password'])
        user.save()
        
        token = Token.objects.create(user=user)
        return Response({'token': token.key, "user": serializer.data}, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
@api_view(['POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def profile(request):
    print(request.user)
    return Response("Estás logeado con: {}".format(request.user.username), status=status.HTTP_200_OK)

def menu(request):
    return render(request, 'menu.html')

def lista_empleados(request):
    empleados = Empleado.objects.all()
    print(empleados)
    return render(request, 'lista_empleados.html', {"empleados": empleados})


@api_view(['POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def crear_empleados(request):
    empleado = Empleado(nombres=request.POST['nombres'], apellidos=request.POST['apellidos'], tipo_identificacion=request.POST['tipoIdentificacion'], identificacion=request.POST['identificacion'],fecha_ingreso=request.POST['fechaIngreso'], salario_mensual=request.POST['salarioMensual'], cargo=request.POST['cargo'], departamento=request.POST['departamento'])
    empleado.save()
    return redirect('/app/')

def eliminar_empleados(request, id_empleado):
    empleado = Empleado.objects.get(id=id_empleado)
    empleado.delete()
    return redirect('/app/empleados/')

def lista_telefonos(request):
    telefonos = Telefono.objects.all()
    print(telefonos)
    return render(request, 'lista_telefonos.html', {"telefonos": telefonos})

def crear_telefonos(request):
    telefono = Telefono(tipo=request.POST['tipo'], numero=request.POST['numero'], indicativo=request.POST['indicativo'], empleado_id=request.POST['empleado'])
    telefono.save()
    return redirect('/app/telefonos/')

def eliminar_telefonos(request, id_telefono):
    telefono = Telefono.objects.get(id=id_telefono)
    telefono.delete()
    return redirect('/app/telefonos/')

def lista_emails(request):
    emails = Email.objects.all()
    print(emails)
    return render(request, 'lista_emails.html', {"emails": emails})

def crear_emails(request):
    email = Email(email=request.POST['email'], empleado_id=request.POST['empleado'])
    email.save()
    return redirect('/app/emails/')

def eliminar_emails(request, id_email):
    email = Email.objects.get(id=id_email)
    email.delete()
    return redirect('/app/emails/')

def mail(request):
    if request.method=="POST":
        email = request.POST.get['email']
        subject = request['subject']
        template = render_to_string('template.html',{

            'email': email,

        })
        email = EmailMessage(
            subject,
            template,
            settings.EMAIL_HOST_USER,
            ['andres.hurtadow6@gmail.com']
        )

        email.fail_silently = False
        email.send()

        messages.success(request, 'Correo Registrado con éxito')
        return redirect('/app/emails/')