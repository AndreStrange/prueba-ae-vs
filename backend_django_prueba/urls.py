from django.contrib import admin
from django.urls import path, include
from empleados.api.views import EmailAPIView

urlpatterns = [
    path('admin/', admin.site.urls),
    #path('app/', include('empleados.urls')),
    path('app/', include('empleados.urls')),
    path('send-email', EmailAPIView.as_view(), name='send-email')
]
