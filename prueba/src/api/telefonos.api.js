import axios from 'axios';

const telefonoAPI = axios.create({
    baseURL: 'http://localhost:8000/app/telefonos/'
})
export const getAllTelefonos = ()  => telefonoAPI.get('/');

export const getTelefono = (id) => telefonoAPI.get('/'+id+'/');

export const crearTelefonos = (telefono)  => telefonoAPI.post('/', telefono);

export const eliminarTelefonos = (id) => telefonoAPI.delete('/'+id)

export const actualizarTelefonos = (id, telefono) => telefonoAPI.put('/'+id+'/', telefono)