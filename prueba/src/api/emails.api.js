import axios from 'axios';

const emailAPI = axios.create({
    baseURL: 'http://localhost:8000/app/emails/'
})
export const getAllEmails = ()  => emailAPI.get('/');

export const getEmail = (id) => emailAPI.get('/'+id+'/');

export const crearEmails = (email)  => emailAPI.post('/', email);

export const eliminarEmails = (id) => emailAPI.delete('/'+id)

export const actualizarEmails = (id, email) => emailAPI.put('/'+id+'/', email)