import axios from 'axios';

const empleadoAPI = axios.create({
    baseURL: 'http://localhost:8000/app/empleados/'
})
export const getAllEmpleados = ()  => empleadoAPI.get('/');

export const getEmpleado = (id) => empleadoAPI.get('/'+id+'/');

export const crearEmpleados = (empleado)  => empleadoAPI.post('/', empleado);

export const eliminarEmpleados = (id) => empleadoAPI.delete('/'+id)

export const actualizarEmpleados = (id, empleado) => empleadoAPI.put('/'+id+'/', empleado)