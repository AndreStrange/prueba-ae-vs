import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import { Empleado } from './pages/Empleado'
import { CrearEmpleado } from './pages/CrearEmpleado';
import {Telefono} from './pages/Telefono';
import {PageTelefono} from './pages/PageTelefonos';
import {Email} from './pages/Email';
import {PageEmail} from './pages/PageEmails';
import { Navegacion } from './components/Navegacion';
import {Toaster} from 'react-hot-toast';

function App() {
  return (

    <BrowserRouter>
      <Navegacion />
      <Routes>
        <Route path="/" element={<Navigate to="/empleados" />} />
        <Route path="/empleados" element={<Empleado />} />
        <Route path="/crear-empleado" element={<CrearEmpleado />} />
        <Route path="/empleados/:id" element={<CrearEmpleado />} />
        <Route path="/telefonos" element={<Telefono />} />
        <Route path="/crear-telefono" element={<PageTelefono />} />
        <Route path="/telefonos/:id" element={<PageTelefono />} />
        <Route path="/emails" element={<Email />} />
        <Route path="/crear-email" element={<PageEmail />} />
        <Route path="/emails/:id" element={<PageEmail />} />
      </Routes>
      <Toaster/>
    </BrowserRouter>
  )
}

export default App