import { useEffect } from 'react';
import { useForm } from 'react-hook-form'
import { actualizarEmpleados, crearEmpleados, eliminarEmpleados, getEmpleado } from '../api/empleados.api';
import { useNavigate, useParams } from 'react-router-dom';
import {toast} from 'react-hot-toast';
import { Container, Box, Typography, TextField, Button, Select, MenuItem } from '@mui/material';

export function CrearEmpleado() {

  const { register, handleSubmit, 
    formState: { errors }, 
    setValue
  } = useForm()

  const navigate = useNavigate()

  const params = useParams()

  const onSubmit = handleSubmit(async (data) => {

    if (params.id) {
      await actualizarEmpleados(params.id, data)
      toast.success('Empleado Actualizado',{
        position: "bottom-right",
        style: {
          background: "#101010",
          color: "#fff"
        }
      })
    } else {
      await crearEmpleados(data)
      toast.success('Empleado Creado',{
        position: "bottom-right",
        style: {
          background: "#101010",
          color: "#fff"
        }
      })
    }
    navigate("/empleados")

  })

useEffect(()=>{
async function cargarEmpleado(){
  if(params.id){
    const res = await getEmpleado(params.id)
    setValue('nombres', res.data.nombres)
    setValue('apellidos', res.data.apellidos)
    setValue('tipo_identificacion', res.data.tipo_identificacion)
    setValue('identificacion', res.data.identificacion)
    setValue('nombres', res.data.nombres)
    setValue('fecha_ingreso', res.data.fecha_ingreso)
    setValue('salario_mensual', res.data.salario_mensual)
    setValue('cargo', res.data.cargo)
    setValue('departamento', res.data.departamento)
}
  }
  cargarEmpleado();
}, [])

  return (
    // <div class="container" >
    //   <div class="row">
    //     <div class="col-md-4 offset-md-4">

    //       <form onSubmit={onSubmit} class="card card-body bg-secondary">

    //         <label for="Nombre">Nombres</label>
    //         <input type="text" name="nombres" placeholder="Ingrese nombres del empleado" class="form-control mb-2" {...register("nombres", { required: true })} />
    //         {errors.nombres && <span>Este campo es requerido</span>}
    //         <label for="Apellidos">Apellidos</label>
    //         <input type="text" name="apellidos" placeholder="Ingrese apellidos del empleado" class="form-control mb-2" {...register("apellidos", { required: true })} />
    //         {errors.apellidos && <span>Este campo es requerido</span>}
    //         <label for="tipoIdentificacion">Tipo de Identificación</label>
    //         <select name="tipo_identificacion" id="" class="form-control mb-2" {...register("tipo_identificacion", { required: true })}>
    //           <option value="NIT">NIT</option>
    //           <option value="CC">CC</option>
    //         </select>
    //         {errors.tipo_identificacion && <span>Este campo es requerido</span>}
    //         <label for="Identificacion">Identificación</label>
    //         <input type="text" name="identificacion" placeholder="Identificación" class="form-control mb-2" {...register("identificacion", { required: true })} />
    //         {errors.identificacion && <span>Este campo es requerido</span>}
    //         <label for="fecha_ingreso">Fecha de Ingreso</label>
    //         <input type="date" name="fechaIngreso" class="form-control mb-2" {...register("fecha_ingreso", { required: true })} />
    //         {errors.fecha_ingreso && <span>Este campo es requerido</span>}
    //         <label for="salario">Salario Mensual</label>
    //         <input type="number" name="salario_mensual" placeholder="Salario" class="form-control mb-2" {...register("salario_mensual", { required: true })} />
    //         {errors.salario_mensual && <span>Este campo es requerido</span>}
    //         <label for="cargo">Cargo</label>
    //         <input type="text" name="cargo" placeholder="Cargo" class="form-control mb-2" {...register("cargo", { required: true })} />
    //         {errors.cargo && <span>Este campo es requerido</span>}
    //         <label for="departamento">Departamento</label>
    //         <input type="text" name="departamento" placeholder="Departamento" class="form-control mb-2" {...register("departamento", { required: true })} />
    //         {errors.departamento && <span>Este campo es requerido</span>}

    //         <button class="btn btn-success">Guardar</button>

    //       </form>

    //       {
    //         params.id && <button onClick={async () => {
    //           const aceptar = window.confirm('¿Estás seguro?')
    //           if (aceptar) {
    //             await eliminarEmpleados(params.id)
    //             toast.success('Empleado Eliminado',{
    //               position: "bottom-right",
    //               style: {
    //                 background: "#101010",
    //                 color: "#fff"
    //               }
    //             });
    //             navigate('/empleados')
                
    //           }
    //         }} class="btn btn-danger">Eliminar</button>
    //       }

    //     </div>
    //   </div>
    // </div>
    <>

<Container maxWidth="sm">
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          mt: 4,
        }}
      >
        <Typography variant="h4" component="h1" mb={2}>
          Empleados
        </Typography>
        <Box component="form" onSubmit={onSubmit} sx={{ width: '100%' }}>
          <TextField
            id="nombres"
            type="text"
            variant="outlined"
            fullWidth
            margin="normal"
            placeholder="Ingrese nombres del empleado"
            error={errors.nombres ? true : false}
            {...register("nombres", { required: true })}
          />
          {errors.nombres && <span>Este campo es requerido</span>}
          <TextField
            id="apellidos"
            type="text"
            variant="outlined"
            fullWidth
            margin="normal"
            placeholder="Ingrese apellidos del empleado"
            error={errors.apellidos ? true : false}
            {...register("apellidos", { required: true })}
          />
          {errors.apellidos && <span>Este campo es requerido</span>}
          <label for="tipoIdentificacion">Tipo de Identificación</label>
            <select name="tipo_identificacion" id="" class="form-control mb-2" {...register("tipo_identificacion", { required: true })}>
             <option value="NIT">NIT</option>
             <option value="CC">CC</option>
           </select>
           {errors.tipo_identificacion && <span>Este campo es requerido</span>}
          {errors.tipo_identificacion && <span>Este campo es requerido</span>}
          <TextField
            id="identificacion"
            type="text"
            variant="outlined"
            fullWidth
            margin="normal"
            placeholder="Identificación"
            error={errors.identificacion ? true : false}
            {...register("identificacion", { required: true })}
          />
          {errors.identificacion && <span>Este campo es requerido</span>}
          <TextField
            id="fecha_ingreso"
            type="date"
            variant="outlined"
            fullWidth
            margin="normal"
            error={errors.fecha_ingreso ? true : false}
            {...register("fecha_ingreso", { required: true })}
          />
          {errors.fecha_ingreso && <span>Este campo es requerido</span>}
          <TextField
            id="salario_mensual"
            type="number"
            variant="outlined"
            fullWidth
            margin="normal"
            placeholder="Salario"
            error={errors.salario_mensual ? true : false}
            {...register("salario_mensual", { required: true })}
          />
          {errors.salario_mensual && <span>Este campo es requerido</span>}
          <TextField
            id="cargo"
            type="text"
            variant="outlined"
            fullWidth
            margin="normal"
            placeholder="Cargo"
            error={errors.cargo ? true : false}
            {...register("cargo", { required: true })}
          />
          {errors.cargo && <span>Este campo es requerido</span>}
          <TextField
            id="departamento"
            type="text"
            variant="outlined"
            fullWidth
            margin="normal"
            placeholder="Departamento"
            error={errors.departamento ? true : false}
            {...register("departamento", { required: true })}
          />
          {errors.departamento && <span>Este campo es requerido</span>}

          <Button type="submit" variant="contained" color="success" fullWidth sx={{ mt: 2 }}>Guardar</Button>

        </Box>
        {params.id && (
          <Button
            variant="contained"
            color="error"
            onClick={async () => {
              const aceptar = window.confirm('¿Estás seguro?');
              if (aceptar) {
                await eliminarEmpleados(params.id);
                toast.success('Empleado Eliminado', {
                  position: "bottom-right",
                  style: {
                    background: "#101010",
                    color: "#fff"
                  }
                });
                navigate('/empleados');
              }
            }}
            fullWidth
            sx={{ mt: 2 }}
          >
            Eliminar
          </Button>
        )}
      </Box>
    </Container>
    </>
  )
}
