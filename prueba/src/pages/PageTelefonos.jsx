import { useEffect } from 'react';
import { useForm } from 'react-hook-form'
import { actualizarTelefonos, crearTelefonos, eliminarTelefonos, getTelefono } from '../api/telefonos.api';
import { useNavigate, useParams } from 'react-router-dom';
import {toast} from 'react-hot-toast';
import { Container, Typography, Box, TextField, Button } from '@mui/material'

export function PageTelefono() {

  const { register, handleSubmit, 
    formState: { errors }, 
    setValue
  } = useForm()

  const navigate = useNavigate()

  const params = useParams()

  const onSubmit = handleSubmit(async (data) => {

    if (params.id) {
      await actualizarTelefonos(params.id, data)
      toast.success('Teléfono Actualizado',{
        position: "bottom-right",
        style: {
          background: "#101010",
          color: "#fff"
        }
      })
    } else {
      await crearTelefonos(data)
      toast.success('Teléfono Creado',{
        position: "bottom-right",
        style: {
          background: "#101010",
          color: "#fff"
        }
      })
    }
    navigate("/telefonos")

  })

useEffect(()=>{
async function cargarTelefono(){
  if(params.id){
    const res = await getTelefono(params.id)
    setValue('tipo', res.data.tipo)
    setValue('numero', res.data.numero)
    setValue('indicativo', res.data.indicativo)
    setValue('empleado', res.data.empleado)
}
  }
  cargarTelefono();
}, [])

  return (
    // <div class="container" >
    //   <div class="row">
    //     <div class="col-md-4 offset-md-4">

    //       <form onSubmit={onSubmit} class="card card-body bg-secondary">

    //         <label for="tipo">Tipo</label>
    //         <select name="tipo" id="" class="form-control mb-2" {...register("tipo", { required: true })}>
    //           <option value="CELL">CELL</option>
    //           <option value="TEL">TEL</option>
    //         </select>
    //         {errors.tipo && <span>Este campo es requerido</span>}
    //         <label for="Identificacion">Número</label>
    //         <input type="text" name="numero" placeholder="Número" class="form-control mb-2" {...register("numero", { required: true })} />
    //         {errors.numero && <span>Este campo es requerido</span>}
    //         <label for="indicativo">Indicativo</label>
    //         <input type="text" name="indicativo" placeholder="Indicativo" class="form-control mb-2" {...register("indicativo", { required: true })} />
    //         {errors.indicativo && <span>Este campo es requerido</span>}
    //         <label for="empleado">Empleado ID</label>
    //         <input type="text" name="empleado" placeholder="ID Empleado" class="form-control mb-2" {...register("empleado", { required: true })} />
    //         {errors.empleado && <span>Este campo es requerido</span>}

    //         <button class="btn btn-success">Guardar</button>

    //       </form>

    //       {
    //         params.id && <button onClick={async () => {
    //           const aceptar = window.confirm('¿Estás seguro?')
    //           if (aceptar) {
    //             await eliminarTelefonos(params.id)
    //             toast.success('Teléfono Eliminado',{
    //               position: "bottom-right",
    //               style: {
    //                 background: "#101010",
    //                 color: "#fff"
    //               }
    //             });
    //             navigate('/telefonos')
                
    //           }
    //         }} class="btn btn-danger">Eliminar</button>
    //       }

    //     </div>
    //   </div>
    // </div>

<>

<Container maxWidth="sm">
  <Box
    sx={{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      mt: 4,
    }}
  >
    <Typography variant="h4" component="h1" mb={2}>
      Telefonos
    </Typography>
    <Box component="form" onSubmit={onSubmit} sx={{ width: '100%' }}>

             <select name="tipo" id="" class="form-control mb-2" {...register("tipo", { required: true })}>
               <option value="CELL">CELL</option>
               <option value="TEL">TEL</option>
             </select>

      <TextField
        id="numero"
        type="text"
        variant="outlined"
        fullWidth
        margin="normal"
        placeholder='Número'
        helperText="Ingrese el número"
        error={errors.numero ? true : false}
        {...register("numero", { required: true })}
      />
      <TextField
        id="indicativo"
        type="text"
        variant="outlined"
        fullWidth
        margin="normal"
        helperText="Ingrese el indicativo"
        placeholder='Indicativo'
        error={errors.indicativo ? true : false}
        {...register("indicativo", { required: true })}
      />
      <TextField
        id="empleado"
        type="number"
        variant="outlined"
        fullWidth
        margin="normal"
        placeholder='Id Empleado'
        helperText="Ingrese el ID del Empleado"
        error={errors.empleado ? true : false}
        {...register("empleado", { required: true })}
      />
      <Button type="submit" variant="contained" color="success" fullWidth sx={{ mt: 2 }}>Guardar</Button>
    </Box>
    {params.id && (
      <Button
      
        variant="contained"
        color="error"
        onClick={async () => {
          const aceptar = window.confirm('¿Estás seguro?');
          if (aceptar) {
            await eliminarTelefonos(params.id);
            toast.success('Email Eliminado', {
              position: "bottom-right",
              style: {
                background: "#101010",
                color: "#fff"
              }
            });
            navigate('/emails');
          }
        }}
        fullWidth
        sx={{ mt: 2 }}
      >
        Eliminar
      </Button>
    )}
  </Box>
</Container>
    </>

  )
}
