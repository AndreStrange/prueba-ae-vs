import { useEffect } from 'react';
import { useForm } from 'react-hook-form'
import { actualizarEmails, crearEmails, eliminarEmails, getEmail } from '../api/emails.api';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-hot-toast';
import { Container, Typography, Box, TextField, Button } from '@mui/material'

export function PageEmail() {

  const { register, handleSubmit,
    formState: { errors },
    setValue
  } = useForm()

  const navigate = useNavigate()

  const params = useParams()

  const onSubmit = handleSubmit(async (data) => {

    if (params.id) {
      await actualizarEmails(params.id, data)
      toast.success('Email Actualizado', {
        position: "bottom-right",
        style: {
          background: "#101010",
          color: "#fff"
        }
      })
    } else {
      await crearEmails(data)
      toast.success('Email Creado', {
        position: "bottom-right",
        style: {
          background: "#101010",
          color: "#fff"
        }
      })
    }

    fetch('http://localhost:8000/send-email', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    navigate("/emails")

  })

  useEffect(() => {
    async function cargarEmail() {
      if (params.id) {
        const res = await getEmail(params.id)
        setValue('email', res.data.email)
        setValue('empleado', res.data.empleado)
      }
    }
    cargarEmail();
  }, [])

  return (
    // <div class="container" >
    //   <div class="row">
    //     <div class="col-md-4 offset-md-4">

    //       <form onSubmit={onSubmit} class="card card-body bg-secondary">

    //         <label for="email">Email</label>
    //         <input type="text" name="email" placeholder="Email" class="form-control mb-2" {...register("email", { required: true })} />
    //         {errors.email && <span>Este campo es requerido</span>}
    //         <label for="empleado">Empleado ID</label>
    //         <input type="text" name="empleado" placeholder="ID Empleado" class="form-control mb-2" {...register("empleado", { required: true })} />
    //         {errors.empleado && <span>Este campo es requerido</span>}

    //         <button class="btn btn-success">Guardar</button>

    //       </form>

    //       {
    //         params.id && <button onClick={async () => {
    //           const aceptar = window.confirm('¿Estás seguro?')
    //           if (aceptar) {
    //             await eliminarEmails(params.id)
    //             toast.success('Email Eliminado', {
    //               position: "bottom-right",
    //               style: {
    //                 background: "#101010",
    //                 color: "#fff"
    //               }
    //             });
    //             navigate('/emails')

    //           }
    //         }} class="btn btn-danger">Eliminar</button>
    //       }

    //     </div>
    //   </div>
    // </div>

    <>

<Container maxWidth="sm">
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          mt: 4,
        }}
      >
        <Typography variant="h4" component="h1" mb={2}>
          Emails
        </Typography>
        <Box component="form" onSubmit={onSubmit} sx={{ width: '100%' }}>
          <TextField
            id="email"
            type="email"
            variant="outlined"
            fullWidth
            margin="normal"
            placeholder='Email'
            helperText="Ingrese un Email válido"
            error={errors.email ? true : false}
            {...register("email", { required: true })}
          />
          <TextField
            id="empleado"
            type="text"
            variant="outlined"
            fullWidth
            margin="normal"
            placeholder='Id Empleado'
            helperText="Ingrese ID del Empleado"
            error={errors.empleado ? true : false}
            {...register("empleado", { required: true })}
          />
          <Button type="submit" variant="contained" color="success" fullWidth sx={{ mt: 2 }}>Guardar</Button>

        </Box>
        {params.id && (
          <Button
            variant="contained"
            color="error"
            onClick={async () => {
              const aceptar = window.confirm('¿Estás seguro?');
              if (aceptar) {
                await eliminarEmails(params.id);
                toast.success('Email Eliminado', {
                  position: "bottom-right",
                  style: {
                    background: "#101010",
                    color: "#fff"
                  }
                });
                navigate('/emails');
              }
            }}
            fullWidth
            sx={{ mt: 2 }}
          >
            Eliminar
          </Button>
        )}
      </Box>
    </Container>
    </>
  )
}
