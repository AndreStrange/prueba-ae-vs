import { Card, CardContent, Typography } from '@mui/material';
import { useNavigate } from 'react-router-dom';

export function EmailsCard({ email }) {
  const navigate = useNavigate();

  return (
    <Card variant="outlined" onClick={() => navigate(`/emails/${email.id}`)} sx={{ cursor: 'pointer', mb: 60, width: 500, margin: 'auto' }}>
      <CardContent>
        <Typography variant="h6" component="h2">{email.email}</Typography>
        <Typography variant="body1">Empleado: {email.empleado}</Typography>
        <hr />
      </CardContent>
    </Card>
  );
}
