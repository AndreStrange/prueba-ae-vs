import { useEffect, useState } from 'react'
import { getAllTelefonos } from '../api/telefonos.api'
import { TelefonosCard } from './TelefonosCard'


export function ListaTelefonos() {

   const [telefonos, setTelefonos] = useState([])

   useEffect(() => {
      async function loadTelefonos() {
         const res = await getAllTelefonos()

         setTelefonos(res.data);

      }
      loadTelefonos();
   }, []);


   return (
      <div style={{ display: 'flex', flexDirection: 'column', gap: '20px', marginTop: 10 }}>
         {telefonos.map((telefono) => (
         <TelefonosCard key={telefono.id} telefono={telefono}/>
      ))}</div>
      )
}