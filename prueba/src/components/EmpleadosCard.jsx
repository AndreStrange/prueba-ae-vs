import { Card, CardContent, Typography } from '@mui/material';
import { useNavigate } from 'react-router-dom';

export function EmpleadosCard({ empleado }) {
  const navigate = useNavigate();

  return (
    <Card variant="outlined" onClick={() => navigate(`/empleados/${empleado.id}`)} sx={{ cursor: 'pointer', mb: 60, width: 500, margin: 'auto' }}>
      <CardContent>
        <Typography variant="h6" component="h2">{empleado.nombres} {empleado.apellidos}</Typography>
        <Typography>{empleado.tipo_identificacion}: {empleado.identificacion}</Typography>
        <Typography>Departamento: {empleado.departamento}</Typography>
        <Typography>Cargo: {empleado.cargo}</Typography>
        <hr />
      </CardContent>
    </Card>
  );
}
