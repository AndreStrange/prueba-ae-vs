import { useEffect, useState } from 'react'
import { getAllEmpleados } from '../api/empleados.api'
import { EmpleadosCard } from './EmpleadosCard'


export function ListaEmpleados() {

   const [empleados, setEmpleados] = useState([])

   useEffect(() => {
      async function loadEmpleados() {
         const res = await getAllEmpleados()
         console.log(res);

         setEmpleados(res.data);

      }
      loadEmpleados();
   }, []);


   return (
      <div style={{ display: 'flex', flexDirection: 'column', gap: '20px', marginTop: 10 }}>
         {empleados.map((empleado) => (
         <EmpleadosCard key={empleado.id} empleado={empleado}/>
      ))}</div>
      )
}