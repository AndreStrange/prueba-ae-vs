import { Link } from 'react-router-dom';
import { Box, Typography } from '@mui/material';

export function Navegacion() {
    return (
        <Box display="flex" justifyContent="space-between" alignItems="center" bgcolor="#f5f5f5" p={2} boxShadow={1} borderRadius={5}>
            <Typography variant="h6" component={Link} to="/empleados" color="primary">Empleados</Typography>
            <Typography variant="h6" component={Link} to="/crear-empleado" color="primary">Crear Empleado</Typography>
            <Typography variant="h6" component={Link} to="/telefonos" color="primary">Teléfono</Typography>
            <Typography variant="h6" component={Link} to="/crear-telefono" color="primary">Crear Teléfono</Typography>
            <Typography variant="h6" component={Link} to="/emails" color="primary">Email</Typography>
            <Typography variant="h6" component={Link} to="/crear-email" color="primary">Crear Email</Typography>
        </Box>
    );
}
