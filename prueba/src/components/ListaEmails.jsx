import { useEffect, useState } from 'react';
import { getAllEmails } from '../api/emails.api';
import { EmailsCard } from './EmailsCard';

export function ListaEmails() {
   const [emails, setEmails] = useState([]);

   useEffect(() => {
      async function loadEmails() {
         const res = await getAllEmails();
         setEmails(res.data);
      }
      loadEmails();
   }, []);

   return (
      <div style={{ display: 'flex', flexDirection: 'column', gap: '20px', marginTop: 10 }}>
         {emails.map((email) => (
            <EmailsCard key={email.id} email={email} />
         ))}
      </div>
   );
}
