import { Card, CardContent, Typography } from '@mui/material';
import { useNavigate } from 'react-router-dom';

export function TelefonosCard({ telefono }) {
  const navigate = useNavigate();

  // Suponiendo que el nombre del empleado está disponible en telefono.empleado.nombre

  return (
    <Card variant="outlined" onClick={() => navigate(`/telefonos/${telefono.id}`)} sx={{ cursor: 'pointer', mb: 60, width: 500, margin: 'auto' }}>
      <CardContent>
        <Typography variant="h6" component="h2">{telefono.tipo}: {telefono.indicativo} {telefono.numero}</Typography>
        <Typography variant="body1">Empleado: {telefono.empleado}</Typography>
        <hr />
      </CardContent>
    </Card>
  );
}
